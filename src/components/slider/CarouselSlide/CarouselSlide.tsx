import React from "react";
import PropTypes from "prop-types";
import { Slide } from "pure-react-carousel";
import "./CarouselSlide.css";

interface CarouselSlideProps {
  index: number;
  image: JSX.Element;
  classToAdd: string;
  callBack: () => void;
}

const enterHover = (e: React.MouseEvent<HTMLDivElement>) => {
  e.preventDefault();
  let imageElement = e.target as HTMLDivElement;
  let slider = document.querySelector(".carousel__slider");
  imageElement.closest("[role='option']").classList.add("is-open");
  if (imageElement.classList.contains("last-slide"))
    slider.classList.add("last__slide");

  slider.classList.add("open-now");
};

const leaveHover = (e: React.MouseEvent<HTMLDivElement>) => {
  e.preventDefault();
  let slider = document.querySelector(".carousel__slider");
  let imageElement = e.target as HTMLDivElement;
  imageElement.closest("[role='option']").classList.remove("is-open");
  slider.classList.remove("open-now");
  slider.classList.remove("last__slide");
};

const CustomCardSlide: React.FC<CarouselSlideProps> = ({
  index,
  image,
  classToAdd,
  callBack,
}) => (
  <Slide index={index} className={classToAdd}>
    <div
      className="slide"
      onMouseEnter={enterHover}
      onMouseLeave={leaveHover}
      onClick={(event) => {
        if (document.querySelector("[role='option'][selected]")) {
          document
            .querySelector("[role='option'][selected]")
            .removeAttribute("selected");
        }
        let imageElement = event.target as HTMLDivElement;
        imageElement
          .closest("[role='option']")
          .setAttribute("selected", "true");
        callBack();
      }}
      style={{ padding: "2px" }}
    >
      {image}
    </div>
  </Slide>
);

CustomCardSlide.propTypes = {
  index: PropTypes.number.isRequired,
};

export default CustomCardSlide;
