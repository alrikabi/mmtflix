import React, { useState } from "react";
import {
  CarouselProvider,
  Slider,
  ButtonBack,
  ButtonNext,
} from "pure-react-carousel";
import "pure-react-carousel/dist/react-carousel.es.css";
import { Placeholder } from "semantic-ui-react";
import CarouselSlide from "../CarouselSlide/CarouselSlide";
import arrow from "../../../assets/Arrow.svg";
import classes from "./Carousel.module.css";

const Carousel = ({
  movieList,
  selectMovie,
  isLoadingMovies,
  visibleSlides,
}) => {
  const [buttonVisible, setButtonVisible] = useState(true);

  // Load images on request, when the user clicks the next arrow
  const lazyLoad = () => {
    let components = document.querySelectorAll(
      ".carousel__slider-tray-wrapper li"
    );

    setButtonVisible(false);

    components.forEach((el) => {
      let image = el.querySelector("img");
      if (!image.src) {
        image.src = image.getAttribute("data-src");
      }
    });
  };

  // Map the movie titles to an array of card slides
  let count = 0;
  const slides = movieList
    .filter((movie) => movie.poster_path)
    .map((movie) => {
      count++;

      let first = count % visibleSlides === 1 ? "first-slide" : "";
      let last = count % visibleSlides === 0 ? "last-slide" : "";

      return (
        <CarouselSlide
          key={count}
          image={
            <img
              src={`https://image.tmdb.org/t/p/w200${movie.poster_path}`}
              className={classes.carouselImage}
              alt={movie.title}
            />
          }
          index={count}
          classToAdd={first + last}
          callBack={() => selectMovie(movie.id)}
        />
      );
    });

  if (isLoadingMovies) {
    for (let i = 1; i <= visibleSlides; i++) {
      count++;
      slides.push(
        <CarouselSlide
          key={count}
          image={
            <Placeholder inverted style={{ height: 200, width: 150 }}>
              <Placeholder.Image />
            </Placeholder>
          }
          index={count}
          classToAdd={""}
          callBack={() => {}}
        />
      );
    }
  }

  // Add extra blank slides so that first and last slides in the carousel stay consistent
  let needExtra = count % visibleSlides;
  let extraSlides = needExtra ? visibleSlides - needExtra : 0;
  for (let i = 1; i <= extraSlides; i++) {
    count++;
    slides.push(
      <CarouselSlide
        key={count}
        image={<img data-src="" className="ui small" width="150px" alt="" />}
        index={count}
        classToAdd={""}
        callBack={() => {}}
      />
    );
  }

  // Create the left and right buttons for the carousel
  let buttons =
    count === 0 ? null : (
      <React.Fragment>
        <ButtonBack
          disabled={buttonVisible}
          children={<img src={arrow} alt="<" />}
        />
        <ButtonNext onClick={lazyLoad} children={<img src={arrow} alt=">" />} />
      </React.Fragment>
    );

  return (
    <React.Fragment>
      <CarouselProvider
        naturalSlideWidth={1}
        naturalSlideHeight={1.75}
        totalSlides={count}
        visibleSlides={visibleSlides}
        step={visibleSlides}
        style={{ width: "100%" }}
        dragEnabled="true"
        className={classes["details-closed"]}
        infinite="true"
      >
        {buttons}
        <Slider>{slides}</Slider>
      </CarouselProvider>
    </React.Fragment>
  );
};

export default Carousel;
