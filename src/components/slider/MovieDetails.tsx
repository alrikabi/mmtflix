import React from "react";

import {
  Transition,
  Dimmer,
  Loader,
  Divider,
  Icon,
  Item,
} from "semantic-ui-react";
import styled from "styled-components";

const StyledItem = styled(Item.Content)`
  &&&& {
    position: relative;
    padding: 20px 0px;
    margin: 0px 20px;
    transition: opacity 250ms;
    @media (max-width: 780px) {
      min-height: 350px;
      margin: 0px;
    }
  }
`;

const StyledItemContainer = styled(Item)`
  @media (max-width: 780px) {
    & > .image {
      display: none !important;
    }
  }
`;

const StyledTransition = styled.div`
  position: relative;
  top: -50px;

  @media (max-width: 780px) {
    .transitionMovie {
      top: -110px !important;
    }
  }
`;

interface MovieDetailsProps {
  movieDetails: {
    genres?: { name: string }[];
    title?: string;
    overview?: string;
    poster_path?: string;
  };
  closeMovie: () => void;
  isLoading: boolean;
}

const MovieDetails: React.FC<MovieDetailsProps> = (props) => {
  let details = (
    <Item.Group
      style={{
        height: "245px",
        backgroundColor: "#131313",
        transitionDuration: "500ms",
      }}
    >
      <Item></Item>
    </Item.Group>
  );
  if (Object.entries(props.movieDetails).length) {
    const genresList = props.movieDetails.genres;

    details = (
      <React.Fragment>
        <Item.Group
          style={{
            position: "relative",
          }}
        >
          <StyledItemContainer>
            <Item.Image
              style={{ width: "180px", minHeight: "280px" }}
              src={`https://image.tmdb.org/t/p/w200/${props.movieDetails.poster_path}`}
            />
            <StyledItem>
              <Item.Header style={{ color: "white", fontSize: "1.6em" }}>
                {props.movieDetails.title}
              </Item.Header>
              <Icon
                style={{ position: "absolute", top: "15px", right: "15px" }}
                inverted
                color="grey"
                name="x"
                size="large"
                link
                onClick={props.closeMovie}
              />
              <Item.Meta
                style={{ overflow: "hidden", marginTop: "0px" }}
              ></Item.Meta>
              <Item.Description
                style={{
                  color: "#828282",
                  fontSize: "1.2em",
                  marginTop: "15px",
                }}
              >
                {genresList.length
                  ? `Genre: ${props.movieDetails.genres
                      .map((genre) => genre.name)
                      .join(", ")}`
                  : ""}
              </Item.Description>
              <Item.Meta
                style={{ overflow: "hidden", marginTop: "0px" }}
              ></Item.Meta>
              <Item.Description
                style={{
                  color: "#828282",
                  fontSize: "1.2em",
                  marginTop: "15px",
                }}
              >
                {props.movieDetails.overview}
              </Item.Description>
            </StyledItem>
          </StyledItemContainer>
        </Item.Group>
        <Divider />
      </React.Fragment>
    );
  }

  return (
    <StyledTransition>
      <Transition visible={props.isLoading} animation="fade" duration={250}>
        <Dimmer
          active={props.isLoading}
          style={{ backgroundColor: "rgb(20, 20, 20)" }}
        >
          <Loader size="big">Loading</Loader>
        </Dimmer>
      </Transition>
      {details}
    </StyledTransition>
  );
};

export default MovieDetails;
