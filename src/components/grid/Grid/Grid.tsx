import React, { useState } from "react";
import GridSlide from "../GridSlide";
import { PaginationBar, paginate } from "../../../pagination/pagination";
import "./Grid.css";

interface GridProps {
  movieList: {}[];
  selectMovie: (id: number) => void;
  isLoadingMovies: boolean;
}

const Grid: React.FC<GridProps> = ({
  movieList,
  selectMovie,
  isLoadingMovies,
}) => {
  const [pageNumber, setPageNumber] = useState(1);

  let count = 0;

  const [totalResults, totalPages, currentPageSlides] = paginate(
    pageNumber,
    movieList
  );

  const moveToNextPage = () => {
    setPageNumber((currPageNum) => {
      return currPageNum < totalPages ? currPageNum + 1 : currPageNum;
    });
  };

  const moveToPrevPage = () => {
    setPageNumber((currPageNum) => {
      return currPageNum > 1 ? currPageNum - 1 : currPageNum;
    });
  };

  const getReleaseYear = (movie: { release_date?: string }): string => {
    const movieYearRegex = /^\d{4}/;
    return movie.release_date.match(movieYearRegex)[0];
  };

  const gridSlides = currentPageSlides.map((movie) => {
    count++;

    const releaseYear = getReleaseYear(movie);

    return (
      <GridSlide
        key={count}
        image={
          <img
            src={`https://image.tmdb.org/t/p/w200${movie.poster_path}`}
            className="carouselImage"
            alt={movie.title}
          />
        }
        title={movie.title}
        year={releaseYear}
        callBack={() => selectMovie(movie.id)}
      />
    );
  });

  return (
    <React.Fragment>
      <PaginationBar
        totalResults={totalResults}
        totalPages={totalPages}
        pageNumber={pageNumber}
        moveToNextPage={moveToNextPage}
        moveToPrevPage={moveToPrevPage}
      />
      <div className="wrapper">{gridSlides}</div>
    </React.Fragment>
  );
};

export default Grid;
