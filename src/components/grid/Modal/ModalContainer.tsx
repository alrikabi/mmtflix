import classes from "./ModalContainer.module.css";

interface ModalProps {
  children: JSX.Element;
}

const Modal: React.FC<ModalProps> = ({ children }) => {
  return (
    <div className={classes.modal}>
      <section className={classes["modal-main"]}>{children}</section>
    </div>
  );
};

export default Modal;
