import React from "react";

import {
  Transition,
  Dimmer,
  Loader,
  Divider,
  Icon,
  Item,
} from "semantic-ui-react";
import styled from "styled-components";
import classes from "./ModalContent.module.css";

const StyledItem = styled(Item.Content)`
  &&&& {
    position: relative;
    padding: 35px;
    margin: 0px 20px;
    transition: opacity 250ms;
    @media (max-width: 780px) {
      min-height: 350px;
      margin: 0px;
    }
  }
`;

const StyledItemContainer = styled(Item)`
  @media (max-width: 780px) {
    & > .image {
      display: none !important;
    }
  }
`;

interface ModalContentProps {
  movieDetails: {
    genres?: { name: string }[];
    title?: string;
    overview?: string;
    poster_path?: string;
  };
  closeMovie: () => void;
  isLoading: boolean;
}

const ModalContent: React.FC<ModalContentProps> = (props) => {
  let details: JSX.Element;

  if (Object.entries(props.movieDetails).length) {
    const genresList = props.movieDetails.genres;

    details = (
      <React.Fragment>
        <Item.Group
          style={{
            position: "relative",
          }}
        >
          <StyledItemContainer>
            <Item.Image
              className={classes.image}
              src={`https://image.tmdb.org/t/p/w200/${props.movieDetails.poster_path}`}
            />
            <StyledItem>
              <Item.Header className={classes.header}>
                {props.movieDetails.title}
              </Item.Header>

              <Icon
                className={classes.icon}
                inverted
                color="grey"
                name="x"
                size="large"
                link
                onClick={props.closeMovie}
              />

              <Item.Description className={classes.description}>
                {genresList.length
                  ? `Genre: ${props.movieDetails.genres
                      .map((genre) => genre.name)
                      .join(", ")}`
                  : ""}
              </Item.Description>

              <Item.Description className={classes.description}>
                {props.movieDetails.overview}
              </Item.Description>
            </StyledItem>
          </StyledItemContainer>
        </Item.Group>
        <Divider />
      </React.Fragment>
    );
  }

  return (
    <>
      <Transition visible={props.isLoading} animation="fade" duration={250}>
        <Dimmer active={props.isLoading} className={classes.dimmer}>
          <Loader size="big">Loading</Loader>
        </Dimmer>
      </Transition>
      {details}
    </>
  );
};

export default ModalContent;
