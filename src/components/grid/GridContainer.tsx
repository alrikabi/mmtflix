import React, { PureComponent } from "react";
import { getMovieDetails } from "../../api/apiEndpoints";
import Grid from "./Grid/Grid";
import ModalContent from "./Modal/ModalContent";
import Modal from "./Modal/ModalContainer";
import styled from "styled-components";

const StyledContainer = styled.div`
  @media (max-width: 780px) {
    width: 80%;
    margin: auto;
  }
`;

interface GridContainerProps {
  movieList: {}[];
  isLoading: boolean;
  slideCount: number;
}

export default class GridContainer extends PureComponent<GridContainerProps> {
  state = { movieDetails: {}, isLoadingMovie: false };

  closeMovie = () => {
    document.querySelector("div[selected]").removeAttribute("selected");

    this.setState({ movieDetails: {} });
  };

  getMovie = async (ID: number) => {
    this.setState({ movieDetails: {}, isLoadingMovie: true });

    const response = await getMovieDetails(ID);

    this.setState({
      movieDetails: response,
      isLoadingMovie: false,
    });
  };

  render() {
    let grid =
      this.props.movieList.length || this.props.isLoading ? (
        <Grid
          movieList={this.props.movieList}
          isLoadingMovies={this.props.isLoading}
          selectMovie={(ID: number) => this.getMovie(ID)}
        />
      ) : null;

    const isMovieSelected = Object.entries(this.state.movieDetails).length;

    let movieDetails =
      isMovieSelected || this.state.isLoadingMovie ? (
        <Modal>
          <ModalContent
            movieDetails={this.state.movieDetails}
            closeMovie={this.closeMovie}
            isLoading={this.state.isLoadingMovie}
          />
        </Modal>
      ) : null;

    return (
      <React.Fragment>
        <StyledContainer>{grid}</StyledContainer>
        {movieDetails}
      </React.Fragment>
    );
  }
}
