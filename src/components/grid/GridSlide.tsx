import React from "react";

interface SlideProps {
  image: JSX.Element;
  title: string;
  year: string;
  callBack: () => void;
}

const enterHover = (e: React.MouseEvent<HTMLDivElement>) => {
  e.preventDefault();
  let imageElement = e.target as HTMLDivElement;
  imageElement.classList.add("is-open");
};

const leaveHover = (e: React.MouseEvent<HTMLDivElement>) => {
  e.preventDefault();
  let imageElement = e.target as HTMLDivElement;
  imageElement.classList.remove("is-open");
};

const CustomCardSlide: React.FC<SlideProps> = ({
  image,
  title,
  year,
  callBack,
}) => {
  return (
    <>
      <div
        className="slide"
        onMouseEnter={enterHover}
        onMouseLeave={leaveHover}
        onClick={(event) => {
          let imageElement = event.target as HTMLDivElement;

          if (document.querySelector("div[selected]")) {
            document.querySelector("div[selected]").removeAttribute("selected");
          }

          imageElement.setAttribute("selected", "true");

          callBack();
        }}
      >
        {image}
        <br />
        <span
          style={{ color: "white", paddingTop: "-200px", position: "relative" }}
        >
          {title}
          <br />
          {year}
        </span>
      </div>
    </>
  );
};

export default CustomCardSlide;
