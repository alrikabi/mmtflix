import React, { PureComponent } from "react";
import { Form, Dropdown } from "semantic-ui-react";
import classes from "./Search.module.css";

interface Props {
  genres: {}[];
  onSubmit: (term: string, genre: number) => void;
}

class SearchBar extends PureComponent<Props> {
  state = { term: "", genre: 0 };

  onFormSubmit = (event: React.FormEvent<EventTarget>) => {
    event.preventDefault();
    this.props.onSubmit(this.state.term, this.state.genre);
  };

  clearInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    e.target.value = "";
  };

  searchPlaceholder = "Search here...";

  resetInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    const inputValue = e.target.value;
    e.target.value = inputValue ? inputValue : this.searchPlaceholder;
  };

  render() {
    return (
      <Form onSubmit={this.onFormSubmit}>
        <div className={classes.searchElements}>
          <div className={classes.searchField}>
            <input
              id={classes.searchInput}
              type="text"
              value={this.state.term ? this.state.term : this.searchPlaceholder}
              onFocus={this.clearInput}
              onBlur={this.resetInput}
              autoComplete="off"
              onChange={(e) =>
                this.setState({ term: e.target.value, genre: 0 })
              }
            />
            <p id={classes.placeholder}>&#61442;</p>
          </div>

          <Dropdown
            selection
            button
            placeholder="Filter by genre"
            options={this.props.genres}
            lazyLoad
            value={this.state.genre}
            onChange={(e, d) => {
              this.setState({ term: this.state.term, genre: d.value }, () =>
                this.props.onSubmit(this.state.term, this.state.genre)
              );
            }}
          />
        </div>
      </Form>
    );
  }
}

export default SearchBar;
