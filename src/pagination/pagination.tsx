import backArrow from "../assets/back-arrow.svg";
import forwardArrow from "../assets/forward-arrow.svg";
import classes from "./pagination.module.css";

export const paginate = (
  pageNumber: number,
  movieList: { poster_path?: string }[]
): [
  totalResults: number,
  totalPages: number,
  currentPageSlides: {
    id?: number;
    title?: string;
    release_date?: string;
    poster_path?: string;
  }[]
] => {
  const moviesWithPosters = movieList.filter((movie) => movie.poster_path);
  const totalResults = moviesWithPosters.length;
  const totalPages = Math.ceil(totalResults / 8);
  const startSlideIndex = (pageNumber - 1) * 8;
  const endSlideIndex = pageNumber * 8;
  const currentPageSlides = moviesWithPosters.slice(
    startSlideIndex,
    endSlideIndex
  );
  return [totalResults, totalPages, currentPageSlides];
};

export const PaginationBar = (props: {
  totalResults: number;
  pageNumber: number;
  totalPages: number;
  moveToPrevPage: () => void;
  moveToNextPage: () => void;
}) => {
  return (
    <div className={classes.main}>
      <div className={classes.results}>{props.totalResults} Results Found</div>
      <div className={classes.navigate}>
        <span>
          Page {props.pageNumber} of {props.totalPages}
        </span>
        <img src={backArrow} alt="back arrow" onClick={props.moveToPrevPage} />
        <img
          src={forwardArrow}
          alt="forward arrow"
          onClick={props.moveToNextPage}
        />
      </div>
    </div>
  );
};
