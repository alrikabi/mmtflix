import React from "react";

import SearchBar from "./components/Search/Search";
import CarouselContainer from "./components/slider/CarouselContainer";
import GridContainer from "./components/grid/GridContainer";
import { getGenres, getMovies } from "./api/apiEndpoints";

import "./App.css";
import logo from "./assets/logo.svg";
class App extends React.Component {
  state = {
    dropDown: [] as {}[],
    movieList: [] as { id: number; title: string }[],
    isLoading: false,
    slideCount: 0,
    view: "slider",
  };

  selectGrid = () => {
    this.setState({ view: "grid" });
  };

  selectSlider = () => {
    this.setState({ view: "slider" });
  };

  moviesList = async (searchTerm: string, genre: number) => {
    await this.setState({ movieList: [], isLoading: false });
    this.setState({ isLoading: true });
    let data = await getMovies(searchTerm, genre);
    this.setState({ movieList: data, isLoading: false });
  };

  countSlides = () => {
    let slideCount = 0;
    if (window.innerWidth >= 1200) {
      slideCount = 4;
    } else if (window.innerWidth >= 992) {
      slideCount = 3;
    } else if (window.innerWidth >= 768) {
      slideCount = 2;
    } else {
      slideCount = 1;
    }
    if (slideCount !== this.state.slideCount) {
      this.setState({ slideCount });
    }
  };

  async componentDidMount() {
    let data = await getGenres();

    const genres = data.reduce(
      (result: {}[], genre: { id: string; name: string }) => {
        result.push({ key: genre.id, value: genre.id, text: genre.name });
        return result;
      },
      [{ key: 0, value: 0, text: "All Genres" }]
    );

    this.setState({ dropDown: genres });
    this.countSlides();
    window.addEventListener("resize", this.countSlides);
  }

  render() {
    let moviesView;

    if (this.state.view === "slider")
      moviesView =
        this.state.movieList.length || this.state.isLoading ? (
          <CarouselContainer
            movieList={this.state.movieList}
            isLoading={this.state.isLoading}
            slideCount={this.state.slideCount}
          />
        ) : null;
    // else return moviesView as a grid
    else
      moviesView =
        this.state.movieList.length || this.state.isLoading ? (
          <GridContainer
            movieList={this.state.movieList}
            isLoading={this.state.isLoading}
            slideCount={this.state.slideCount}
          />
        ) : null;

    return (
      <div className="container">
        <div className="header">
          <div>
            <img src={logo} alt="MMTFlix Logo" className="logo" />
          </div>
          <div className="searchArea">
            <SearchBar
              genres={this.state.dropDown}
              onSubmit={(searchTerm: string, genre: number) => {
                this.moviesList(searchTerm, genre);
              }}
            />
          </div>
        </div>

        <div className="moviesContainer">{moviesView}</div>

        <div id="footer">
          Browse Movies As:
          <br />
          <span className="footerLink" onClick={this.selectGrid}>
            Grid
          </span>
          <span className="footerSeprarator">|</span>
          <span className="footerLink" onClick={this.selectSlider}>
            Slider
          </span>
        </div>
      </div>
    );
  }
}

export default App;
