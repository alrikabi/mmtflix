import { getMovies, getGenres, getMovieDetails } from "./apiEndpoints";
import axios from "axios";

describe("API", () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it("fetches movies by the searched term", async () => {
    const data = {
      results: [{ id: 284521 }],
    };

    axios.get.mockImplementationOnce(() => Promise.resolve({ data }));

    const moviesList = await getMovies("globe");

    expect(moviesList[0]).toHaveProperty("id");
    expect(axios.get).toHaveBeenCalledTimes(1);
  });

  it("fetches a list of all available genres", async () => {
    const data = {
      genres: [{ id: 1, name: "Action" }],
    };

    axios.get.mockImplementationOnce(() => Promise.resolve({ data }));

    const genresList = await getGenres();

    expect(genresList[0]).toMatchObject({ id: 1, name: "Action" });
    expect(axios.get).toHaveBeenCalledTimes(1);
  });

  it("fetches the details of a specific movie", async () => {
    const data = { id: 1 };

    axios.get.mockImplementationOnce(() => Promise.resolve({ data }));

    const movieDetails = await getMovieDetails(1);

    expect(movieDetails).toMatchObject({ id: 1 });
    expect(axios.get).toHaveBeenCalledTimes(1);
  });
});
