import axios from "axios";

export const moviesAPI = axios.create({
  baseURL: "https://api.themoviedb.org/3/search/movie",
});

export const movieAPI = axios.create({
  baseURL: "https://api.themoviedb.org/3/movie",
});

export const genresAPI = axios.create({
  baseURL: "https://api.themoviedb.org/3/genre/movie/list",
});
