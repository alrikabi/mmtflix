import { moviesAPI, movieAPI, genresAPI } from "./axiosInstances";

const reportError = (error: {
  response: { data: { errors: [] }; status: number };
}) => {
  if (error.response) {
    error.response.data.errors.map((errorMessage) => console.log(errorMessage));
    console.log("Error Status:", error.response.status);
  }
};

export const getMovies = async (searchTerm: string, genre: number) => {
  const response = await moviesAPI
    .get("", {
      params: {
        query: searchTerm,
        language: "en-US",
        page: 1,
        include_adult: false,
        api_key: process.env.REACT_APP_API_KEY,
      },
    })
    .catch(function (error) {
      reportError(error);
    });

  let results = response && response.data.results;

  if (results && genre) {
    results = results.filter((record: { genre_ids: number[] }) =>
      record.genre_ids.includes(genre)
    );
  }

  return results || [];
};

export const getGenres = async () => {
  const response = await genresAPI
    .get("", {
      params: {
        language: "en-US",
        api_key: process.env.REACT_APP_API_KEY,
      },
    })
    .catch(function (error) {
      reportError(error);
    });

  return (response && response.data.genres) || [];
};

export const getMovieDetails = async (ID: number) => {
  const response = await movieAPI
    .get(`${ID}`, {
      params: {
        language: "en-US",
        api_key: process.env.REACT_APP_API_KEY,
      },
    })
    .catch(function (error) {
      reportError(error);
    });

  return (response && response.data) || {};
};
