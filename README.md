# Movies Search Page

This is a Netflix-like page that allows users to search for movies from the <a href="https://developers.themoviedb.org/3" target="_blank">The Movie Database</a> (version 3).

Here is a <a href="https://www.loom.com/share/43a7964ce91e4508ba74074a83e4b4a0" target="_blank">video explainer</a> about it.

**Running The App:**

- Create **_.env_** file in the root directory of the app with the following property:
  **_REACT_APP_API_KEY=_**
  You can obtain the value of this property by signing up to <a href="https://developers.themoviedb.org/3" target="_blank">The Movie Database</a>.
- Install the dependencies: **_npm i_**
- Do: **_npm start_**

**Run The Tests:**

For demo purposes, I have limited the unit tests to api calls, and the end-to-end tests to the grid view part of the app.

- For the unit test, run: **_npm test_**
- For the E2E test, run: **_npm run cypress_**
  Then run the test: **_gridView.spec.js_**

**Notes:**

- I have used a combination of CSS Modules and styled components. Where there wasn't possible to use them, I reverted back to normal CSS.
- The api response retreived from "The Movie Database" doesn't include the movie director and cast details.

![](./design.png)
