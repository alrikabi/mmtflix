describe("Grid view", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("should display movie details in a modal", () => {
    cy.get("#footer > :nth-child(2)").click();
    cy.findByRole("textbox").type("sun").type("{enter}");
    cy.get(".wrapper > :nth-child(1)").click();
    cy.get(".item").should("exist");
  });
});
